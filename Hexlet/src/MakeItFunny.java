public class MakeItFunny {

    public static String encrypt(String str) {
        String result = "";
        for (int i = 0; i < str.length(); i++) {
            if (i%2 == 0) {
                result += str.charAt(i);
            }
        }
        return result;
    }
}
